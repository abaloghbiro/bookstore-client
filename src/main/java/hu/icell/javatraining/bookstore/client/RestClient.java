package hu.icell.javatraining.bookstore.client;

import java.util.Set;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.Response;

import hu.icell.javatraining.bookstore.model.Book;
import hu.icell.javatraining.bookstore.model.Customer;

public class RestClient {

	public static void main(String[] args) {

		Client restClient = ClientBuilder.newClient();
		WebTarget target = restClient.target("http://localhost:8080/bookstore/store/");
		WebTarget resourceTarget = target.path("customer/1");
		Response response = resourceTarget.request("application/xml").get();
		Customer customer = response.readEntity(Customer.class);
		System.out.println("ID : "+ customer.getId());
		System.out.println("NAME : " + customer.getFirstName());
		System.out.println("LAST NAME : " + customer.getLastName());
		System.out.println("--------------------------------------------");
		Set<Link> links = response.getLinks();
		System.out.println("The following books are owned by this customer :");
		for (Link link : links) {
			if (!link.getRel().equals("self")) {
				Book book = restClient.target(link).request().get(Book.class);
				System.out.println(book.getTitle());
				System.out.println(book.getAuthor());
				System.out.println(book.getIsbn());
				System.out.println("This book is located at :");
				System.out.println(link.getUri());
				System.out.println("--------------------------------------------");
			}
		}
	}
}
